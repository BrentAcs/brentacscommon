﻿namespace BrentAcs.Common.Drawing
{
  public class Sphere : Ellipsoid
  {
    public Sphere(double radius)
      : base(radius, radius, radius)
    {
    }
  }
}