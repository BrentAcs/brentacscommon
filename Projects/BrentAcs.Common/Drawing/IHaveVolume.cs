﻿namespace BrentAcs.Common.Drawing
{
  public interface IHaveVolume
  {
    double Volume { get; }
  }
}