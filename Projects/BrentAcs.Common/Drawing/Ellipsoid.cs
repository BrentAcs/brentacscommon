﻿using System;

namespace BrentAcs.Common.Drawing
{
  public class Ellipsoid : IHaveVolume
  {
    // TODO: implement .Equals() and related

    public Ellipsoid(double a = 0.0, double b = 0.0, double c = 0.0)
    {
      A = a;
      B = b;
      C = c;
    }

    public double A { get; set; }
    public double B { get; set; }
    public double C { get; set; }

    public double Volume => (4.0 / 3.0) * Math.PI * A * B * C;

    public bool ContainsPoint(Point3D point, Point3D centeredAt = null)
    {
      centeredAt = centeredAt ?? new Point3D();

      // Ref:  https://www.geeksforgeeks.org/check-if-a-point-is-inside-outside-or-on-the-ellipse/
      double position = (Math.Pow((point.X - centeredAt.X), 2) / Math.Pow(A, 2)) +
                        (Math.Pow((point.Y - centeredAt.Y), 2) / Math.Pow(B, 2)) +
                        (Math.Pow((point.Z - centeredAt.Z), 2) / Math.Pow(C, 2));

      if (position > 1.0)
        return false;

      return true;
    }

    public override string ToString()
    {
      return $"[ A:{A:0.0000}, B:{B:0.0000}, C:{C:0.0000}]";
    }
  }
}