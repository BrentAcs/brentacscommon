﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace BrentAcs.Common.Storage.Mongo
{
  public interface IRequireConfigVerification
  {
    // marker interface
  }

  public interface IMongoRepository<T> : IDisposable, IRequireConfigVerification
  {
    IMongoDatabase Database { get; }
    IMongoCollection<T> Collection { get; }

    //void InsertOne(T item, InsertOneOptions options = null, CancellationToken token = default);
    //Task InsertOneAsync(T item, InsertOneOptions options = null, CancellationToken token = default);
    //void InsertMany(IEnumerable<T> items, InsertManyOptions options = null, CancellationToken token = default);
    //Task InsertManyAsync(IEnumerable<T> items, InsertManyOptions options = null, CancellationToken token = default);

    T FindFirstOrDefaultSync(FilterDefinition<T> filter, FindOptions<T, T> options = null, CancellationToken token = default);

    Task<T> FindFirstOrDefaultAsync(FilterDefinition<T> filter, FindOptions<T, T> options = null, CancellationToken token = default);

    //T FindOne(Expression<Func<T, bool>> expression, FindOptions<T, T> options = null, CancellationToken token = default);

    //READ
    //- find(filter, options )
    //  - findOne(filter, options )

    //UPDATE
    //- updateOne(filter, data, options )
    //  - updateMany(filter, data, options )
    //  - replaceOne(filter, data, options )

    //DELETE
    //- deleteOne(filter, options )
    //  - deleteMany(filter, options )
  }
}