﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;

namespace BrentAcs.Common.Core.Storage.Mongo
{
  public abstract class MongoRepository<T> : IMongoRepository<T> where T : class
  {
    private static IMongoClient _mongoClient;
    private static object _mongoLock = new Object();

    private readonly IConfiguration _configuration;
    private readonly ILogger _logger;

    protected static readonly CreateIndexOptions<T> DefaultNonUniqueCaseInsensitiveIndexOptions = new CreateIndexOptions<T>
    {
      Collation = new Collation("en_US",
        false,
        new Optional<CollationCaseFirst?>(CollationCaseFirst.Off),
        new Optional<CollationStrength?>(CollationStrength.Primary))
    };

    protected static readonly CreateIndexOptions<T> DefaultUniqueCaseInsensitiveIndexOptions = new CreateIndexOptions<T>
    {
      Collation = new Collation("en_US",
        false,
        new Optional<CollationCaseFirst?>(CollationCaseFirst.Off),
        new Optional<CollationStrength?>(CollationStrength.Primary)),
      Unique = true
    };

    public static readonly FindOptions<T, T> DefaultCaseInsensitiveFindOptions = new FindOptions<T, T>
    {
      Collation = new Collation("en_US", strength: CollationStrength.Primary)
    };

    protected MongoRepository(IConfiguration configuration = null, 
      ILogger logger = null, 
      string connectionString = null, 
      string databaseName = null, 
      string collectionName = null)
    {
      _configuration = configuration;
      _logger = logger;

      connectionString ??= _configuration?[ConnectionStringConfigKey];
      databaseName ??= _configuration?[DatabaseNameConfigKey];

      if (string.IsNullOrWhiteSpace(connectionString))
      {
        throw new ArgumentException($"Missing connection string, consider setting configuration key '{ConnectionStringConfigKey}'.");
      }
      if (string.IsNullOrWhiteSpace(databaseName))
      {
        throw new ArgumentException($"Missing database name, consider setting configuration key '{DatabaseNameConfigKey}'.");
      }
      if (string.IsNullOrWhiteSpace(collectionName))
      {
        throw new ArgumentException("Missing collection name.");
      }

      if (_mongoClient == null)
      {
        lock (_mongoLock)
        {
          if (_mongoClient == null)
          {
            logger?.LogInformation($"connecting to Mongo w/ '{connectionString}'");

            _mongoClient = new MongoClient(connectionString);
          }
        }
      }

      logger?.LogInformation($"getting database {databaseName}");
      Database = _mongoClient.GetDatabase(databaseName);
      bool collectionExists = Database
        .ListCollectionNames()
        .ToList()
        .Any(col => collectionName.Equals(col, StringComparison.CurrentCultureIgnoreCase));
      if (!collectionExists)
      {
        logger?.LogInformation($"creating collection {collectionName}");
        Database.CreateCollection(collectionName);
        Collection = Database.GetCollection<T>(collectionName);
        InitializeCollection(Collection);
      }
      else
      {
        logger?.LogInformation($"getting collection {collectionName}");
        Collection = Database.GetCollection<T>(collectionName);
      }
    }

    // used by derived/concrete classes to specify which keys in Configuration to use. Only required if derived classes don't allow for passing in values.
    protected virtual string ConnectionStringConfigKey => null;
    protected virtual string DatabaseNameConfigKey => null;

    public IMongoDatabase Database { get; }
    public IMongoCollection<T> Collection { get; }

    // NOTE: simply a forwarding method, consider removing
    //public void InsertOne(T item, InsertOneOptions options = null, CancellationToken token = default)
    //{
    //  Collection.InsertOne(item, options, token);
    //}

    // NOTE: simply a forwarding method, consider removing
    //public async Task InsertOneAsync(T item, InsertOneOptions options = null, CancellationToken token = default)
    //{
    //  await Collection.InsertOneAsync(item, options, token);
    //}

    // NOTE: simply a forwarding method, consider removing
    //public void InsertMany(IEnumerable<T> items, InsertManyOptions options = null, CancellationToken token = default)
    //{
    //  Collection.InsertMany(items, options, token);
    //}

    // NOTE: simply a forwarding method, consider removing depending how options plays out.
    //public async Task InsertManyAsync(IEnumerable<T> items, InsertManyOptions options = null, CancellationToken token = default)
    //{
    //  await Collection.InsertManyAsync(items, options, token);
    //}

    public virtual T FindFirstOrDefaultSync(FilterDefinition<T> filter, FindOptions<T, T> options = null, CancellationToken token = default)
    {
      var results = Collection.FindSync(filter, options, token);
      return results.FirstOrDefault();
    }

    public virtual Task<T> FindFirstOrDefaultAsync(FilterDefinition<T> filter, FindOptions<T, T> options = null, CancellationToken token = default)
    {
      var results = Collection.FindSync(filter, options, token);
      return results.FirstOrDefaultAsync(cancellationToken: token);
    }

    public void Dispose()
    {
      // nothing unmanaged to dispose of. if so, see:  https://docs.microsoft.com/en-us/dotnet/standard/garbage-collection/implementing-dispose
    }

    protected virtual void InitializeCollection(IMongoCollection<T> collection)
    {
    }
  }
}
