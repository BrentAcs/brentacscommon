﻿using Newtonsoft.Json;

namespace BrentAcs.Common.Core.Drawing
{
  public class Cuboid
  {
    // TODO: implement .Equals() and related

    public Cuboid(double width = 0.0, double length = 0.0,  double height = 0.0)
    {
      Width = width;
      Length = length;
      Height = height;
    }

    public double Width { get; set; }   // X axis
    public double Length { get; set; }  // Y axis
    public double Height { get; set; }  // Z axis

    [JsonIgnore]
    public Point3D Center => new Point3D(  Width/2, Length / 2, Height /2);

    public override string ToString()
    {
      return $"[ Width:{Width:0.0000}, Length:{Length:0.0000}, Height:{Height:0.0000}]";
    }
  }
}