﻿using System;
using BrentAcs.Common.Core.Utilities;

namespace BrentAcs.Common.Core.Drawing
{
  public class Point3D
  {
    public Point3D(double x=0.0, double y=0.0, double z=0.0)
    {
      X = x;
      Y = y;
      Z = z;
    }

    public double X { get; set; }
    public double Y { get; set; }
    public double Z { get; set; }

    public override string ToString()
    {
      return $"[ {X:0.0000}, {Y:0.0000}, {Z:0.0000}]";
    }

    public override bool Equals(object obj)
    {
      Point3D that = obj as Point3D;
      
      return !ReferenceEquals(null, that) &&
             X.Equals(that.X) &&
             Y.Equals(that.Y) &&
             Z.Equals(that.Z);
    }

    public override int GetHashCode()
    {
      unchecked
      {
        const int HashingBase = (int)2166136261;
        const int HashingMultiplier = 16777619;

        int hash = HashingBase;
        hash = (hash * HashingMultiplier) ^ (!ReferenceEquals(null, X) ? X.GetHashCode() : 0);
        hash = (hash * HashingMultiplier) ^ (!ReferenceEquals(null, Y) ? Y.GetHashCode() : 0);
        hash = (hash * HashingMultiplier) ^ (!ReferenceEquals(null, Z) ? Z.GetHashCode() : 0);
        return hash;
      }
    }

    public static bool operator ==(Point3D lhs, Point3D rhs)
    {
      if (ReferenceEquals(lhs, rhs))
        return true;

      if (ReferenceEquals(null, lhs))
        return false;

      return lhs.Equals(rhs);
    }

    public static bool operator !=(Point3D lhs, Point3D rhs)
    {
      return !(lhs == rhs);
    }

    public double DistanceTo(Point3D that)
    {
      var result = 0.0;

      var a = X - that.X;
      var b = Y - that.Y;
      var c = Z - that.Z;

      result = Math.Sqrt((a * a) + (b * b) + (c * c));
      return result;
    }

    public static Point3D CreateRandomInside(Ellipsoid ellipsoid, IRng rng = null)
    {
      if (rng == null)
        rng = Rng.Instance;

      Point3D result = new Point3D(
        rng.Next(-ellipsoid.A, ellipsoid.A),
        rng.Next(-ellipsoid.B, ellipsoid.B),
        rng.Next(-ellipsoid.C, ellipsoid.C));
      while (!ellipsoid.ContainsPoint(result))
      {
        result = new Point3D(
          rng.Next(-ellipsoid.A, ellipsoid.A),
          rng.Next(-ellipsoid.B, ellipsoid.B),
          rng.Next(-ellipsoid.C, ellipsoid.C));
      }

      return result;
    }

    public static Point3D CreateRandomInside(Cuboid cuboid, IRng rng = null)
    {
      rng = rng ?? Rng.Instance;

      var result = new Point3D(
        rng.Next(0.0, cuboid.Width),
        rng.Next(0.0, cuboid.Length),
        rng.Next(0.0, cuboid.Height));

      return result;
    }
  }
}
