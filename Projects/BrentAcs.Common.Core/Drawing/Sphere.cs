﻿namespace BrentAcs.Common.Core.Drawing
{
  public class Sphere : Ellipsoid
  {
    public Sphere(double radius)
      : base(radius, radius, radius)
    {
    }
  }
}