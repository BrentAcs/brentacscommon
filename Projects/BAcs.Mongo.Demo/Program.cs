﻿using System;
using System.Threading.Tasks;
using System.Xml;
using BrentAcs.Common.Core.Storage.Mongo;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using Moq;

namespace BAcs.Mongo.Demo
{
  class Program
  {
    private static readonly SamplePoco[] SampleData = new SamplePoco[]
    {
      new SamplePoco("Jim","Billings", 48, "Archy dude"),
      new SamplePoco("Connor", "Mann", 21, "Spicoli Wanna-be"),
      new SamplePoco("Rich", "Mann", 52, "Big Dick"),
      new SamplePoco("Brent", "Acs", 47, "Sr. Dev"),
      new SamplePoco("Lucas", "Mann", 21, "Bears QB"),
    };

    static void Main(string[] args)
    {
      Mock<IConfiguration> configMock = new Mock<IConfiguration>();
      configMock.SetupGet(c => c[ConfigKeys.MongoDbConnectionString]).Returns("mongodb://localhost:27017/");
      configMock.SetupGet(c => c[ConfigKeys.MongoDbDatabaseName]).Returns("BrentAcsCommonTest");

      // create repo w/ configuration object
      var repo = new SamplePocoRepository(configMock.Object);

      // create repo w/ supplied values
      //var repo = new SamplePocoRepository(connectionString: "mongodb://localhost:27017/", databaseName: "BrentAcsCommonTest");

      DemonstratePopulation(repo);
      DemonstrateFindFirstOrDefault(repo);
      DemonstrateFindAsync(repo);

      Console.WriteLine("Done");
    }

    private static void DemonstratePopulation(SamplePocoRepository repo)
    {
      try
      {
        // seed database (will need to use external tool to clear it)
        repo.Collection.InsertMany(SampleData);

        // attempt to insert a duplicate document. will throw exception when indexes are unique
        repo.Collection.InsertOne(SampleData[2]);
      }
      catch (MongoWriteException ex)
      {
        // This exception IS expected when indexes are unique
        Console.WriteLine($"unable to insert one, {ex.Message}");
      }
      catch (Exception ex)
      {
        // erm, what happened?
        Console.WriteLine($"unable to insert one, {ex.Message}");
      }
    }

    private static void DemonstrateFindFirstOrDefault(SamplePocoRepository repo)
    {
      // use filter and our repo's FindFirstOrDefault to find one
      FilterDefinition<SamplePoco> filter = Builders<SamplePoco>
        .Filter
        .And(
          Builders<SamplePoco>.Filter.Eq(p => p.LastName, "ACS"),
          Builders<SamplePoco>.Filter.Eq(p => p.FirstName, "brent"));

      var options = SamplePocoRepository.DefaultCaseInsensitiveFindOptions;
      options.Projection = "{_id:0}";

      var poco = repo.FindFirstOrDefaultSync(filter, SamplePocoRepository.DefaultCaseInsensitiveFindOptions);

      Console.WriteLine($"Find first or default: {poco}");
      Console.WriteLine();
    }

    private static void DemonstrateFindAsync(SamplePocoRepository repo)
    {
      // use filter and repo's collection to find several. 
      // NOTE: cursor returns batches, not single docs. verbose code to demonstrate this
      FilterDefinition<SamplePoco> filter2 = Builders<SamplePoco>
        .Filter
        .Eq(p => p.LastName, "mann");

      IAsyncCursor<SamplePoco> cursor = repo.Collection
        .FindAsync(filter2, SamplePocoRepository.CaseInsensitiveSortByFirstNameFindOptions).Result;
      while (cursor.MoveNext())
      {
        foreach (var pocoDoc in cursor.Current)
        {
          Console.WriteLine(pocoDoc);
        }
      }
    }
  }

  public class SamplePocoRepository : MongoRepository<SamplePoco>
  {
    public SamplePocoRepository(IConfiguration configuration = null, ILogger<SamplePocoRepository> logger = null, string connectionString = null, string databaseName = null)
      : base(configuration, logger, connectionString, databaseName, CollectionNames.SamplePocos)
    {
    }

    // explore using 'canned' find options, not sure if I like this
    // purpose: DRY'ing up creation of potential duplicates
    public static readonly FindOptions<SamplePoco, SamplePoco> CaseInsensitiveFindOptions = DefaultCaseInsensitiveFindOptions;
    //public static readonly FindOptions<SamplePoco, SamplePoco> CaseInsensitiveFindOptions = new FindOptions<SamplePoco, SamplePoco>
    //{
    //  Collation = new Collation("en_US", strength: CollationStrength.Primary)
    //};

    public static readonly FindOptions<SamplePoco, SamplePoco> CaseInsensitiveSortByFirstNameFindOptions = new FindOptions<SamplePoco, SamplePoco>
    {
      Collation = new Collation("en_US", strength: CollationStrength.Primary),
      Sort = Builders<SamplePoco>.Sort.Ascending(p => p.FirstName),
      Projection = "{_id:0}"
    };

    // Indicate which keys should be used to query Configuration
    protected override string ConnectionStringConfigKey => ConfigKeys.MongoDbConnectionString;
    protected override string DatabaseNameConfigKey => ConfigKeys.MongoDbDatabaseName;

    protected override void InitializeCollection(IMongoCollection<SamplePoco> collection)
    {
      base.InitializeCollection(collection);

      var index = new CreateIndexModel<SamplePoco>("{ FirstName : 1, LastName : 1 }", DefaultUniqueCaseInsensitiveIndexOptions);
      Collection.Indexes.CreateOne(index);
    }
  }

  public class ConfigKeys
  {
    public const string MongoDbConnectionString = "ConnectionStrings:MongoDB";
    public const string MongoDbDatabaseName = "MongoDB:BrentAcsCommonTest:DatabaseName";
  }

  public class CollectionNames
  {
    public const string SamplePocos = "SamplePocos";
  }

  //[BsonIgnoreExtraElements]
  public class SamplePoco
  {
    public SamplePoco(string fn = "", string ln = "", int a = 0, string jt = "")
    {
      FirstName = fn;
      LastName = ln;
      Age = a;
      JobTitle = jt;
    }

    public string FirstName { get; set; }
    public string LastName { get; set; }
    public int Age { get; set; }
    public string JobTitle { get; set; }

    public override string ToString()
    {
      return $"{FirstName} {LastName} is {Age} years old. Title: {JobTitle}";
    }
  }
}
